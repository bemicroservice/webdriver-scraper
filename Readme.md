# Webdriver Scraper
Let's try some web scraping! It is a technique to harvest data from website without access to its server APIs.Web scraping is a legal way to obtain online public information for its own purpose, just be careful about authors' policy in the case of commercial projects. 

There are many tools and libraries useful for the purpose, for example python beautifulsoup. The main idea is to get webpage HTML output and seek information inside the code structure. This could work with server side rendered content (SSR), but what if you want to harvest dynamic single page application (SPA)? Server generated HTML, include almost no content, which is delivered by JavaScript code executions by browser interactions. 

For that case, you can use web E2E (testing) tool, that can open a browser aand simulate real user behaviour. And this here comes **Selenium Webdriver** library. 

## Testing Case
For the sample I have chosen BBC career page, which has more than 200 open positions, but broken filter. This app scrap and save all open positions for the purpose later analysis by keywords etc. 

## Technology Stack
- Node.js (Typescript)
- Selenium Webdriver
- MongoDB

## ENV Variables
- DB_STRING - Connection string (MongoDB)
- DB_NAME - Database name

## Install
- Download and install Node.js with NPM
- Download webdriver by prefered browser. I have used safari driver, that is already included in Mac Computers.
- Install and run MongoDB
- Clone the repository
- Run with required ENV variables