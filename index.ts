
import { Builder, WebDriver } from "selenium-webdriver";
import { saveScrapedUrlsToDb, produceDatabaseClient, saveJobDocument, cleanOldDbData } from "./services/DataStorage";
import { JobDescription, scrapAllJobUrls, scrapJobData } from "./services/ScrapUtils";

const DB_STRING = process.env.DB_STRING as string
const DB_NAME = process.env.DB_NAME as string


(async function example() {
  let browser: WebDriver = await new Builder().forBrowser('safari').build()
  const {client, db} = await produceDatabaseClient(DB_STRING, DB_NAME)
  
  try {
      await cleanOldDbData(db)

      const scrapedJobLinks = await scrapAllJobUrls(browser)
      await saveScrapedUrlsToDb(scrapedJobLinks, db)

      for (const url of scrapedJobLinks.urls) {
        const singleJobData: JobDescription = await scrapJobData(url, browser)
        await saveJobDocument(singleJobData, db)
      }

    }
  finally {
    client.close()
    await browser.quit();
  }
})();


