import { By, WebDriver, WebElement, WebElementPromise } from "selenium-webdriver"

/**
 * Collection of job links
 */
export interface UrlScrapCollection {
  updated: Date
  urls: Array<string>
}

/**
 * One single job data document
 */
export interface JobDescription {
  updated: Date
  header: string
  shortDescription: string
  link: string
}


/**
 * Check, if scraped content includes required web element
 * @param findingBlockPromise 
 * @returns 
 */
const elementExist = async (findingBlockPromise: WebElementPromise): Promise<boolean> => {
  try {
    await findingBlockPromise
    return true
  } catch (err) {
    return false
  }
}

/**
 * Scrap all job links from the website
 * @param browser Webdriver browser instance (safari, chrome, firefox etc.)
 * @param stopAtPage For the case, when you need to scrap only limited number of pages
 * @returns Record about all scraped job links
 */
export const scrapAllJobUrls = async (browser: WebDriver, stopAtPage?: number): Promise<UrlScrapCollection> => {
  let currentNumber = 1
  let urlsSum: Array<string> = new Array()

  while (true) {
    await browser.get(`https://careerssearch.bbc.co.uk/jobs/search/-1/${currentNumber}`);

    const checkAlert = await elementExist(browser.findElement(By.css(".alert-foo")))
    if (checkAlert || (stopAtPage && currentNumber > stopAtPage))
      break

    console.log(`Actual page is ${currentNumber}`)

    const jobLinks: WebElement[] = await browser.findElement(By.className("jobs")).findElements(By.css("a"))
    const urls: Array<string> = await Promise.all(jobLinks.map(async (element) => await element.getAttribute("href")))
    urlsSum = [...urlsSum, ...urls]

    currentNumber++
  }

  return { urls: urlsSum, updated: new Date() }
}

/**
 * Scrap data about the job from its detail page. If detail page contain mistake, is skipped
 * @param jobUrl Linkd url to one single job detail page
 * @param browser Webdriver browser instance (safari, chrome, firefox etc.)
 * @returns Jobs detail data document
 */
export const scrapJobData = async (jobUrl: string, browser: WebDriver): Promise<JobDescription> => {

  try {
    await browser.get(jobUrl)

    const header = await browser.findElement({ css: "h1" }).getText()
    const shortDescription = await browser.findElement(By.css(".feature-text")).findElement(By.css("p")).getText()

    console.log(`${header}`)

    return {
      header,
      shortDescription,
      link: jobUrl,
      updated: new Date()
    }
  } catch (error) {
    console.warn(`Element error - ${jobUrl}`)
    return {
      header: "Element Issue",
      shortDescription: "",
      link: jobUrl,
      updated: new Date()
    }
  }

}