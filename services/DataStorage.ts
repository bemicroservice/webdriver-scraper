import { Db, MongoClient } from "mongodb";
import { JobDescription, UrlScrapCollection } from "./ScrapUtils";


const COLLECTION_URLS = "urls"
const COLLECTION_JOBS = "jobs"

/**
 * Build mongo connection client
 * @param connectionString 
 * @param databaseName 
 * @returns MongoDB client and its database instance object
 */
export const produceDatabaseClient = async (connectionString: string, databaseName: string) => {
  const client = await new MongoClient(connectionString).connect()
  const db = client.db(databaseName)
  return { client, db }
}

/**
 * Clear old webscraping database record
 * @param db Database instance
 * @returns 
 */
export const cleanOldDbData = async (db: Db) => await db.dropDatabase()

/**
 * Save scraped job links into collection
 * @param jobLinks List of job URLs
 * @param db Database instance
 * @returns 
 */
export const saveScrapedUrlsToDb = async (jobLinks: UrlScrapCollection, db: Db) =>
  db.collection(COLLECTION_URLS).insertOne(jobLinks)


/**
 * Read last saved job links from database
 * @param db Database instance
 * @returns List of job links
 */
export const loadScrapedUrlsFromDb = async (db: Db): Promise<UrlScrapCollection> => {
  const record: unknown = await db.collection(COLLECTION_URLS).findOne({})
  return <UrlScrapCollection>record
}

/**
 * Save single job data document into the database
 * @param job Job data document
 * @param db Database instance
 * @returns 
 */
export const saveJobDocument = async (job: JobDescription, db: Db) => {
  job.updated = new Date()
  return db.collection(COLLECTION_JOBS).insertOne(job)
}

/**
 * Load single job data document
 * @param db Database instance
 * @returns Document with data about one single job
 */
export const loadJobDocument = async (db: Db): Promise<JobDescription> => {
  const record: unknown = await db.collection(COLLECTION_JOBS).findOne({})
  return <JobDescription>record
}