import { expect } from "chai";
import { Builder, WebDriver } from "selenium-webdriver"
import { JobDescription, scrapAllJobUrls, scrapJobData, UrlScrapCollection } from "../services/ScrapUtils";

const TEST_URL = "https://careerssearch.bbc.co.uk/jobs/job/Fulfilment-Executive/58334"
let testBrowser: WebDriver;


describe("Check url extraction", async () => {

  before(() => {
    testBrowser = new Builder().forBrowser('safari').build()
  })

  it("Extract urls from one site", async () => {
    const jobLinks: UrlScrapCollection = await scrapAllJobUrls(testBrowser, 1)
    expect(jobLinks.urls).to.have.lengthOf(10)
    console.log(jobLinks)
  })

  it("Extract job data", async () => {
    const {header, shortDescription, link}: JobDescription = await scrapJobData(TEST_URL, testBrowser)
    expect(header).contain("Fulfilment Executive")
    expect(shortDescription).contain("To support the delivery of sales across")
    expect(link).equals(TEST_URL)
  })

  after(async () => await testBrowser.quit())

})