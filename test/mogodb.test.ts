import { expect } from "chai";
import { describe, it, after, before } from "mocha";
import { Db, MongoClient } from "mongodb";
import { saveScrapedUrlsToDb, produceDatabaseClient, loadJobDocument, loadScrapedUrlsFromDb, saveJobDocument } from "../services/DataStorage";
import { JobDescription } from "../services/ScrapUtils";

const testingConnectionString = process.env.DB_STRING as string
const DB_NAME = "webscrap_test"
const TEST_URL_STUB = "https://aaa.com"
const TEST_JOB_STUB : JobDescription = {
  header: "Test Header",
  link: "https://aaa.com",
  shortDescription: "Some cool description",
  updated: new Date()
}

let db: Db, client: MongoClient;

describe("Test Mongo Integration", async () => {

  before(async () => {
    const { db: database, client: clientMongo } = await produceDatabaseClient(testingConnectionString, DB_NAME)
    db = database
    client = clientMongo
  })

  it("save urls to database", async () => {
    const saved = await saveScrapedUrlsToDb({urls: [TEST_URL_STUB], updated: new Date()}, db)
    expect(saved).to.haveOwnProperty("acknowledged", true)
  })

  it("get urls back from database", async () => {
    const dbResult = await loadScrapedUrlsFromDb(db)
    expect(dbResult.urls[0]).to.equal(TEST_URL_STUB)
  })

  it("save job description to database", async () => {
    const dbResult = await saveJobDocument(TEST_JOB_STUB, db)
    expect(dbResult).to.haveOwnProperty("acknowledged", true)
  })

  it("get job description back from database", async () => {
    const dbResult = await loadJobDocument(db)
    expect(dbResult.header).to.equal(TEST_JOB_STUB.header)
    expect(dbResult.link).to.equal(TEST_JOB_STUB.link)
    expect(dbResult.shortDescription).to.equal(TEST_JOB_STUB.shortDescription)
    expect(dbResult).haveOwnProperty("updated")
  })

  after(async () => {
    await db.dropDatabase()
    client.close()
  })
})